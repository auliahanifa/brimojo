<?php 
namespace App\Models;
use CodeIgniter\Model;

class ServerModel extends Model
{
    protected $table = 'server_list';

    public function getServer($id = false)
    {
        if($id === false){
            return $this->findAll();
        } else {
            return $this->getWhere(['server_ip' => $id]);
        }
    }

    public function saveServer($data)
    {
        $builder = $this->db->table($this->table);
        return $builder->insert($data);
    }

    public function deleteServer($id)
    {
        $builder = $this->db->table($this->table);
        return $builder->delete(['server_ip' => $id]);
    }

    public function getTotalServers()
    {
        return $this->db->table('server_list')->countAll();
    }
}