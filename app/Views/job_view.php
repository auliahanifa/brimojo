<div class="container pt-5">
    <h5 ></h5>
    <div class="row">
        <div class="col-sm-3 col-md-6 col-lg-8">
            <input class="form-control" id="myInput" type="text" placeholder="Search..">
        </div>
        <div class="col-sm-3 col-md-6 col-lg-4">
            <a href="<?= base_url('job/add');?>" class="btn btn-default">add new job</a> <!-- manggil function addNew dr controller job-->
        </div>
    </div>
    
    <br>
    <h4><?= $totalJobs?> jobs</h4>
    <h4><?= $totalServers?> servers</h4>
    <div class="card">
        <div class="card-header bg-info text-white">
            <h4 class="card-title"><?= $title; ?></h4>
            
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>No. </th>
                            <th>Ip address</th>
                            <th>Job Name</th>
                            <th>Migration Status</th>
                            <th>Aksi</th>
                        </tr> 
                    </thead>
                    <tbody id="myTable">
                        <?php $no=1; foreach($job as $isi){?> <!-- define job dr model $data['job'] ke variable isi-->
                            <tr>
                                <td><?= $no;?></td>
                                <td><?= $isi['server_ip'];?></td>
                                <td><?= $isi['job_name'];?></td>
                                <td><?= $isi['status_migrasi'];?></td>
                                <td>
                                    <a href="<?= base_url('job/edit/'.$isi['job_id']);?>" 
                                    class="btn btn-success">
                                    Edit</a>
                                    <a href="<?= base_url('job/delete/'.$isi['job_id']);?>" 
                                    onclick="javascript:return confirm('Are you sure to delete this job?')"
                                    class="btn btn-danger">
                                    Delete</a>

                                </td>
                            </tr>
                        <?php $no++;}?>
                    </tbody>  

                </table>
            </div>
        </div>
    </div>
</div>

<script>
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>
