<div class="container p-5">
    <a href="<?= base_url('job');?>" class="btn btn-secondary mb-2">Kembali</a>
    <div class="card">
        <div class="card-header bg-info text-white">
            <h4 class="card-title">Edit Job: <?= $job->job_name;?></h4>
        </div>
        <div class="card-body">
            <form method="post" action="<?= base_url('job/update');?>">
                <div class="form-group">
                    <label for="">Job name</label>
                    <input type="text" value="<?= $job->job_name;?>" name="jobname" required class="form-control">
                </div>
                <div class="form-group">
                    <label for="">IP address</label>
                    <input type="text" value="<?= $job->server_ip;?>" name="serverid" required class="form-control">
                </div>
                <div class="form-group">
                    <label for="sel1">Status migrasi</label>
                    <select class="form-control" id="sel1" name="migrasi">
                        <option><?= $job->status_migrasi;?></option>
                        <option>in progress</option>
                        <option>done</option>
                    </select>
                </div>
                <!-- <div class="form-group">
                    <label for="">Status running</label>
                    <input type="text" value="" name="running" class="form-control">
                </div> -->
                <input type="hidden" value="<?= $job->job_id;?>" name="jobid">
                <button class="btn btn-success">save Data</button>
            </form>
            
        </div>
    </div>
</div>