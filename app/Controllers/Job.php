<?php 
namespace App\Controllers;
use CodeIgniter\Controller;
use App\Models\JobModel;
use App\Models\ServerModel;

class Job extends Controller
{   
    public function index()
    {
        $model = new JobModel; //manggil model JobModel
        $server = new ServerModel;
        $data['title'] = 'List Job';
        $data['job'] = $model->getJob(); //manggil function getJob dr model
        $data['totalJobs'] = $model->getTotalJobs();
        $data['totalServers'] = $server->getTotalServers();
        echo view('header_view', $data);
        echo view('job_view', $data);
        echo view('footer_view', $data);
        
    }

    public function add()
    {
        $server = new ServerModel;
        $data['servers'] = $server->getServer();

        $data['title'] = 'Create new data job';
        echo view('header_view', $data);
        echo view('tambahjob_view', $data);
        echo view('footer_view', $data);
        
    }

    public function createNewJob()
    {
        $model = new JobModel;
        
        $data = array (
            'job_name' => $this->request->getPost('jobname'),
            'server_ip' => $this->request->getPost('serverid'),
            'status_migrasi' => $this->request->getPost('migrasi'),
            // 'running_status' => $this->request->getPost('running')
        );
        $model->saveJob($data);
        echo '
        <script> 
            alert("new job successfully added!");
            window.location="'.base_url('job').'"
        </script>       
        ';
    }

    public function edit($id)
    {
        $model = new JobModel;
        $getJob = $model->getJob($id)->getRow();
        if(isset($getJob))
        {
            $data['job'] = $getJob;
            $data['title'] = 'Edit '.$getJob->job_id;

            echo view('header_view', $data);
            echo view('edit_view', $data);
            echo view('footer_view', $data);
            
        } else {
            echo 
            '<script>
                alert("Job dengan ID '.$id.' tidak ditemukan.");
                window.location="'.base_url('job').'"
            </script>';
        }

    }

    public function update()
    {
        $model = new JobModel;
        $id = $this->request->getPost('jobid');
        $data = array (
            'job_name' => $this->request->getPost('jobname'),
            'server_ip' => $this->request->getPost('serverid'),
            'status_migrasi' => $this->request->getPost('migrasi'),
            // 'running_status' => $this->request->getPost('running')
        );
        $model->editJob($data, $id);
        echo '
        <script> 
            alert("edit job '.$data['job_name'].' is success!");
            window.location="'.base_url('job').'"
        </script>       
        ';       
    }

    public function delete($id)
    {
        $model = new JobModel;
        $getJob = $model->getJob($id)->getRow();
        if(isset($getJob)){
            $model->deleteJob($id);
            
            echo '
            <script> 
                alert("successfully delete job.");
                window.location="'.base_url('job').'"
            </script>
            ';
        } else {
            echo '
            <script> 
                alert("Failed to remove the Job.");
                window.location="'.base_url('job').'"
            </script>
            ';
        }
    }
}